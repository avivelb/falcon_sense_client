import 'dart:convert';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:falcon_sense/classes/Sensor.dart';
import 'package:falcon_sense/classes/StringReference.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/widget/custom_dropdown.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class SensorMenu extends StatefulWidget {
  final PageData data;
  const SensorMenu({Key? key, required this.data}) : super(key: key);
  @override
  _SensorMenuState createState() => _SensorMenuState(data: this.data);
}

class _SensorMenuState extends State<SensorMenu> {
  _SensorMenuState({required this.data});
  final PageData data;
  TextEditingController _editSensorNameController = new TextEditingController();
  TextEditingController _editSensorLocationController =
      new TextEditingController();
  TextEditingController _addSensorNameController = new TextEditingController();
  TextEditingController _addSensorLocationController =
      new TextEditingController();
  TextEditingController _addSensorGPIOController = new TextEditingController();
  TextEditingController _addSensorWirelessLengthController =
      new TextEditingController();
  Point? _lastPoint;
  StringReference _selectedType = new StringReference("");
  Future<List> _initRegisteredSensors() async {
    List<Sensor> sensorsTemp = [];
    Utils.showLoaderDialog(context);
    String response = await data.client!.send("${data.token}#GETSENS");
    Navigator.pop(context);
    List sensorsRaw = jsonDecode(response.split("#")[1]);
    sensorsRaw.forEach((element) {
      sensorsTemp.add(Sensor.fromJson(element));
    });
    return sensorsTemp;
  }

  @override
  initState() {
    super.initState();
  }

  _popupMenu(Sensor sensor) async {
    int? _value;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("View"),
          value: 1,
        ),
        PopupMenuItem(
          child: Text("Edit"),
          value: 2,
        ),
        PopupMenuItem(
          child: Text("Delete"),
          value: 3,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value);
    switch (_value) {
      case 1:
        _viewSensorDialog(sensor);
        break;
      // Edit sensor details
      case 2:
        _editSensorDialog(sensor);
        break;
      case 3:
        _deleteSensorDialog(sensor);
    }
  }

  SpeedDial _buildSensorsSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.add, color: Colors.white),
          backgroundColor: Colors.red,
          onTap: () => _addSensorDialog(),
          label: 'Add sensor',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.red,
        ),
      ],
    );
  }

  void _editSensorDialog(Sensor sensor) async {
    showGeneralDialog(
      barrierLabel: "Edit Sensor",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 300,
                child: ListView(
                    children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Center(
                        child: Text(sensor.name),
                      ),
                    ),
                    ListTile(
                      title: TextField(
                        controller: _editSensorNameController,
                        decoration: InputDecoration(
                          labelText: "Name",
                          hintText: sensor.name,
                        ),
                      ),
                    ),
                    ListTile(
                        title: TextField(
                      controller: _editSensorLocationController,
                      decoration: InputDecoration(
                        labelText: "Location",
                        hintText: sensor.location,
                      ),
                    )),
                    Center(
                      child: FlatButton(
                        onPressed: () => {
                          _editSensor(sensor, _editSensorNameController.text,
                              _editSensorLocationController.text),
                          _editSensorNameController.text = "",
                          _editSensorLocationController.text = "",
                          Navigator.of(context).pop(),
                        },
                        child: Text("Save"),
                        color: Colors.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.black)),
                      ),
                    )
                  ],
                ).toList()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ));
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  void _deleteSensorDialog(Sensor sensor) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        _deleteSensor(sensor);
        Navigator.of(_customContext!).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Sensor Deletion"),
      content: Text("Are you sure you want to delete the selected sensor?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  void _deleteSensor(Sensor sensor) async {
    String toSend;
    if (sensor.type == "Physical") {
      toSend = "${data.token}#DELSENPHYS#${sensor.gpIO}";
    } else if (sensor.type == "Wireless") {
      toSend = "${data.token}#DELSENWIRL#${sensor.length}";
    } else {
      Utils.toastShow("Couldn't find selected sensor");
      return;
    }
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      setState(() {
        data.sensors?.remove(sensor);
      });
    } else {
      Utils.toastShow("Error occurred: $resultPart");
    }
  }

  void _addSensorDialog() async {
    _selectedType.str = "Physical";
    showGeneralDialog(
      barrierLabel: "Add Sensor",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 500,
                child: ListView(
                    children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Center(
                        child: Text("New Sensor"),
                      ),
                    ),
                    ListTile(
                      title: TextField(
                        controller: _addSensorNameController,
                        decoration: InputDecoration(
                          labelText: "Name",
                          hintText: "Name",
                        ),
                      ),
                    ),
                    ListTile(
                        title: TextField(
                      controller: _addSensorLocationController,
                      decoration: InputDecoration(
                        labelText: "Location",
                        hintText: "Location",
                      ),
                    )),
                    CustomDropDown(
                        hint: "Type",
                        choices: ["Physical", "Wireless"],
                        choice: _selectedType),
                    ListTile(
                        title: TextFormField(
                      controller: _addSensorGPIOController,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: "GPIO",
                        hintText: "Leave blank if wireless",
                      ),
                    )),
                    ListTile(
                        title: TextFormField(
                      controller: _addSensorWirelessLengthController,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        labelText: "Wireless Length",
                        hintText: "Leave blank if physical",
                      ),
                    )),
                    Center(
                      child: FlatButton(
                        onPressed: () => {
                          _addSensor(
                              _addSensorNameController.text,
                              _addSensorLocationController.text,
                              _selectedType.str,
                              _addSensorGPIOController.text,
                              _addSensorWirelessLengthController.text),
                          _addSensorGPIOController.text = "",
                          _addSensorNameController.text = "",
                          _addSensorLocationController.text = "",
                        },
                        child: Text("Save"),
                        color: Colors.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.black)),
                      ),
                    )
                  ],
                ).toList()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ));
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  void _viewSensorDialog(Sensor sensor) async {
    showGeneralDialog(
      barrierLabel: "View Sensor",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 450,
                child: ListView(
                    children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Center(
                        child: Text(sensor.name),
                      ),
                    ),
                    ListTile(
                      title: Text("Name: ${sensor.name}"),
                    ),
                    ListTile(
                      title: Text("Location: ${sensor.location}"),
                    ),
                    ListTile(
                      title: Text("GPIO: ${sensor.gpIO}"),
                    ),
                    ListTile(
                      title: Text("PIN: ${sensor.pin}"),
                    ),
                    ListTile(
                      title: Text("Type: ${sensor.type}"),
                    ),
                    ListTile(
                      title: Text(
                          "Wireless Length: ${(sensor.type == "Wireless") ? sensor.length : "None (Physical)"}"),
                    ),
                  ],
                ).toList()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ));
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  void _addSensor(String name, String location, String type, String GPIO,
      String wirelessLength) async {
    String? toSend;
    // A lot of checks before actually sending to be sure the data is correct.
    if (type.isEmpty) {
      Utils.toastShow("You must specify the type of the sensor");
      return;
    } else if (name.isEmpty || location.isEmpty) {
      Utils.toastShow("You must specify name and location");
      return;
    }
    if (type == "Physical") {
      if (GPIO.isEmpty) {
        Utils.toastShow("You must specify GPIO on Physical sensor");
        return;
      }
      toSend = "${data.token}#ADDSENPHYS#$name#$location#$GPIO";
    } else if (type == "Wireless") {
      if (wirelessLength.isEmpty) {
        Utils.toastShow("You must specify Wireless Length on Wireless sensor");
        return;
      }
      toSend = "${data.token}#ADDSENWIRL#$name#$location#$wirelessLength";
    }
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend!);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      // Ask for the sensors again.
      new Future<List>.delayed(
              new Duration(seconds: 0), () => _initRegisteredSensors())
          .then((List value) {
        setState(() {
          data.sensors = value as List<Sensor>;
        });
      });
    } else {
      Utils.toastShow("Error: $resultPart");
    }
  }

  void _editSensor(Sensor sensor, String newName, String newLocation) async {
    if (newName.isEmpty || newName.isEmpty) {
      Utils.toastShow("Please fill all of the fields");
      return;
    }
    Utils.showLoaderDialog(context);
    String response = await data.client!
        .send("${data.token}#EDITSEN#${sensor.gpIO}#$newName#$newLocation");
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      Utils.toastShow("Sensor edited successfully");
      setState(() {
        sensor.name = newName;
        sensor.location = newLocation;
      });
    } else {
      Utils.toastShow("Error: $resultPart");
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          drawer: PagesDrawer(
            caller: "Sensors",
            data: data,
          ),
          floatingActionButton: _buildSensorsSpeedDial(),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Colors.blueGrey, Colors.redAccent])),
              child: data.sensors == null
                  ? null
                  : Center(
                      child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                            onTapDown: (TapDownDetails details) =>
                                _lastPoint = Utils.onTap(details),
                            child: Card(
                                child: ListTile(
                              leading: Image.asset("assets/sensor.png"),
                              title: Text(data.sensors!.elementAt(index).name),
                              subtitle:
                                  Text(data.sensors!.elementAt(index).location),
                              onTap: () => Utils.toastShow(
                                data.sensors!.elementAt(index).toString(),
                              ),
                              onLongPress: () =>
                                  _popupMenu(data.sensors!.elementAt(index)),
                            )));
                      },
                      itemCount: data.sensors!.length,
                    ))),
        ));
  }
}
