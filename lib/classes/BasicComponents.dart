import 'dart:convert';
import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Schedule.dart';
import 'package:falcon_sense/classes/Sensor.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:flutter/material.dart';

class BasicComponents {
  PageData data;

  BasicComponents(this.data);

  Future<void> initComponents() async {
    if (data.sensors == null) {
      data.sensors = await _initRegisteredSensors() as List<Sensor>;
    }
    if (data.groups == null) {
      data.groups = await _initRegisteredGroups() as List<Group>;
    }
    if (data.schedules == null) {
      data.schedules = await _initRegisteredSchedules() as List<Schedule>;
    }
    await _initHumidity();
  }

  Future<void> refreshComponents() async {
    data.sensors = await _initRegisteredSensors() as List<Sensor>;
    data.groups = await _initRegisteredGroups() as List<Group>;
    data.schedules = await _initRegisteredSchedules() as List<Schedule>;
    await _initHumidity();
  }

  Future<Map> _initSensorMap(Map sensorMap) async {
    data.sensors!.forEach((element) {
      sensorMap[element.name] = element;
    });
    return sensorMap;
  }

  Future<void> refreshHumidity() async {
    await _initHumidity();
  }

  Future<void> _initHumidity() async {
    var response =
        (await data.client!.send("${data.token}#${Client.getHumidity}"))
            .split("#");
    sharedPrefs.temperature = double.parse(response[1]);
    sharedPrefs.humidity = double.parse(response[2]);
    sharedPrefs.isCooling = (response[3] == "True");
  }

  Future<List> _initRegisteredGroups() async {
    Map<String, Sensor> _sensorMap = new Map();
    new Future<Map>.delayed(
            new Duration(seconds: 0), () => _initSensorMap(_sensorMap))
        .then((value) => {
              _sensorMap = value as Map<String, Sensor>,
            });
    String response =
        await data.client!.send("${data.token}#${Client.getGroups}");
    List groups = jsonDecode(response.split("#")[1]);
    List<Group> tempGroups = [];
    groups.forEach((group) {
      tempGroups.add(Group.fromJson(group, _sensorMap));
    });
    return tempGroups;
  }

  Future<List> _initRegisteredSchedules() async {
    List<Schedule> schedules = [];
    String response =
        await data.client!.send("${data.token}#${Client.getScheduleDaily}");
    List shdRaw = jsonDecode(response.split("#")[1]);
    shdRaw.forEach((schedule) {
      schedules.add(Schedule.fromJson(schedule));
    });
    return schedules;
  }

  Future<List> _initRegisteredSensors() async {
    List<Sensor> sensorsTemp = [];
    String response =
        await data.client!.send("${data.token}#${Client.getSensors}");
    List sensorsRaw = jsonDecode(response.split("#")[1]);
    sensorsRaw.forEach((element) {
      sensorsTemp.add(Sensor.fromJson(element));
    });
    return sensorsTemp;
  }
}
