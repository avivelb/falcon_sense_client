import 'dart:convert';
import 'package:falcon_sense/classes/BasicComponents.dart';
import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Schedule.dart';
import 'package:falcon_sense/classes/Sensor.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:falcon_sense/classes/StringReference.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/pages/mainmenu.page.dart';
import 'package:falcon_sense/widget/login_button.dart';
import 'package:falcon_sense/widget/username_input.dart';
import 'package:falcon_sense/widget/password_input.dart';
import 'package:falcon_sense/widget/appname_text.dart';
import 'package:falcon_sense/widget/vertical_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  bool _isRegister = false;
  // Initialized only once, run through the entire app lifecycle.
  static final PageData pageData = new PageData(
    token: null,
    client: null,
    sensors: null,
    groups: null,
    schedules: null,
  );
  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      if (sharedPrefs.cached!) authenticate(context, true);
    });
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance!.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      // These are all the cases in which the app is considered not active.
      case AppLifecycleState.detached:
      case AppLifecycleState.paused:
      case AppLifecycleState.inactive:
        sharedPrefs.isAppActive = false;
        break;
      default:
        sharedPrefs.isAppActive = true;
    }
    debugPrint("Current is AppActive: ${sharedPrefs.isAppActive}");
    sharedPrefs.reload();
  }

  void authenticate(BuildContext context, bool automatic) async {
    String response, username, password;
    if (pageData.client == null) pageData.client = new Client(context);
    //if (!pageData.client.isConnected) await pageData.client.connect();
    if (!automatic &&
        (usernameController.text.isEmpty || passwordController.text.isEmpty)) {
      Utils.toastShow("Please provide valid credentials");
      return;
    }
    // Check if sharedPrefs has the info...
    if (sharedPrefs.cached! &&
        sharedPrefs.username != null &&
        sharedPrefs.passHash != null) {
      username = sharedPrefs.username!;
      password = sharedPrefs.passHash!;
    } else {
      username = usernameController.text;
      if (_isRegister) {
        password = passwordController.text;
      } else {
        password = Utils.cryptoSha512(passwordController.text);
      }
    }
    if (pageData.token == null) {
      Utils.showLoaderDialog(context);
      response = await pageData.client!.send('AUTH#$username:$password');
      Navigator.pop(context);
      // debugPrint(response);
      var responseFields = response.split("#");
      String responseAppToken = responseFields[1];
      // If the authentication is ok then the response should be the token.
      if (responseAppToken != Client.authenticationError) {
        pageData.token = responseAppToken;
        sharedPrefs.appToken = responseAppToken;
        sharedPrefs.username = username;
        if (_isRegister) {
          sharedPrefs.passHash = Utils.cryptoSha512(password);
        } else {
          sharedPrefs.passHash = password;
        }
        // If the auth is successful, we'll get also the notification token for listening for new messages
        sharedPrefs.notificationToken = responseFields[2];
        await BasicComponents(pageData).initComponents();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MainMenu(
                      data: pageData,
                    )));
      } else {
        Utils.toastShow("Authentication failed!");
      }
    } else {
      await BasicComponents(pageData).initComponents();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MainMenu(
                    data: pageData,
                  )));
    }
  }

  @override
  Widget build(BuildContext context) {
    pageData.client = new Client(context);
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blueGrey, Colors.redAccent]),
            ),
            child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(children: <Widget>[
                      VerticalText(),
                      AppNameText(),
                    ]),
                    UsernameInput(
                      usernameController: usernameController,
                    ),
                    PasswordInput(
                      passwordController: passwordController,
                    ),
                    Row(children: <Widget>[
                      Checkbox(
                          value: _isRegister,
                          onChanged: (bool? value) => this.setState(() {
                                _isRegister = value!;
                              })),
                      Text(
                        'Register',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white70,
                        ),
                      ),
                      LoginButton(
                        onPressed: () {
                          authenticate(context, false);
                        },
                      ),
                    ]),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
