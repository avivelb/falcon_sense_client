import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Schedule.dart';
import 'package:falcon_sense/classes/StringReference.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/widget/custom_dropdown.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:falcon_sense/widget/responsive_tile.dart';
import 'package:flutter/material.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:falcon_sense/classes/Client.dart';

class Scheduler extends StatefulWidget {
  final PageData data;
  const Scheduler({Key? key, required this.data}) : super(key: key);
  @override
  _SchedulerState createState() => _SchedulerState(
        data: this.data,
      );
}

class _SchedulerState extends State<Scheduler> {
  _SchedulerState({required this.data});
  final PageData data;
  StringReference? _selectedGroup = new StringReference("");
  StringReference? _selectedDay = new StringReference("");
  StringReference? _selectedAction = new StringReference("");
  StringReference? _selectedTimeFormat = new StringReference("");
  Point? _lastPoint;
  void _addScheduleAskDialog(
      String groupName, String day, String action, String timeFormat) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        _addSchedule(groupName, day, action, timeFormat);
        Navigator.of(_customContext!).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Add Schedule"),
      content: Text("Are you sure you want to add this schedule?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  void _deleteScheduleAskDialog(Schedule schedule) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        _deleteSchedule(schedule);
        if (_customContext != null) {
          Navigator.of(_customContext!).pop();
        }
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Delete Schedule"),
      content: Text("Are you sure you want to delete this schedule?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  _deleteSchedule(Schedule schedule) async {
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(
        "${data.token}#${Client.deleteScheduleDaily}#${schedule.groupName}#${schedule.day}#${schedule.action}#${schedule.time}");
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart != Client.serverOK) {
      Utils.toastShow("Error: $resultPart");
      return;
    }
    setState(() {
      data.schedules!.remove(schedule);
    });
  }

  void _popupMenu(Schedule schedule) async {
    int? _value;
    const int deleteSchedule = 1;
    const int editSchedule = 2;
    const int viewSchedule = 3;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("Delete Schedule"),
          value: deleteSchedule,
        ),
        PopupMenuItem(
          child: Text("Edit Schedule (Not implemented yet)"),
          value: editSchedule,
        ),
        PopupMenuItem(
          child: Text("View Schedule"),
          value: viewSchedule,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value);
    switch (_value) {
      case deleteSchedule:
        _deleteScheduleAskDialog(schedule);
        break;
      case editSchedule:
        //await Utils.askDialog(
        //   context, "Group Arm", "Are you sure?", _scheduleGroup, group);
        break;
      case viewSchedule:
        _viewScheduleDialog(schedule);
        break;
    }
  }

  void _viewScheduleDialog(Schedule schedule) async {
    showGeneralDialog(
      barrierLabel: "View Schedule",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 450,
                child: ListView(
                    children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Center(
                        child: Text(schedule.groupName),
                      ),
                    ),
                    ListTile(
                      title: Text("Name: ${schedule.groupName}"),
                    ),
                    ListTile(
                      title: Text("Day: ${schedule.day}"),
                    ),
                    ListTile(
                      title: Text("Time: ${schedule.time}"),
                    ),
                    ListTile(
                      title: Text("Action: ${schedule.action}"),
                    ),
                  ],
                ).toList()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ));
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  void _addSchedule(
      String groupName, String day, String action, String timeFormat) async {
    if (groupName == null ||
        day == null ||
        action == null ||
        timeFormat == "None") {
      Utils.toastShow("Please provide valid entries!");
      return;
    }
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(
        "${data.token}#${Client.addScheduleDaily}#$groupName#$day#$action#$timeFormat");
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart != Client.serverOK) {
      Utils.toastShow("Error: $resultPart");
      return;
    }
    setState(() {
      data.schedules!.add(Schedule(groupName, day, timeFormat, action));
    });
  }

  SpeedDial _buildGroupSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.add, color: Colors.white),
          backgroundColor: Colors.red,
          onTap: () => _addScheduleDialog(),
          label: 'Add Schedule',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.red,
        ),
      ],
    );
  }

  List<String> _getGroupNames() {
    List<String> groups = [];
    data.groups!.forEach((group) {
      groups.add(group.name!);
    });
    return groups;
  }

  void _addScheduleDialog() async {
    List<String> groups = _getGroupNames();
    _selectedGroup!.str = groups[0];
    _selectedDay!.str = "Sunday";
    _selectedAction!.str = "Arm";
    showGeneralDialog(
      barrierLabel: "Add Schedule",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 350,
                child: ListView(
                    children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    CustomDropDown(
                        hint: "Group",
                        choices: groups,
                        choice: _selectedGroup!),
                    CustomDropDown(
                        hint: "Day",
                        choices: [
                          "Sunday",
                          "Monday",
                          "Tuesday",
                          "Wednesday",
                          "Thursday",
                          "Friday",
                          "Saturday"
                        ],
                        choice: _selectedDay!),
                    ResponsiveTimePicker(
                      title: "Time",
                      selectedTimeFormat: _selectedTimeFormat!,
                    ),
                    CustomDropDown(
                        hint: "Action",
                        choices: [
                          "Arm",
                          "Disarm",
                        ],
                        choice: _selectedAction!),
                    Center(
                      child: FlatButton(
                        onPressed: () => _addScheduleAskDialog(
                            _selectedGroup!.str,
                            _selectedDay!.str,
                            _selectedAction!.str,
                            _selectedTimeFormat!.str),
                        child: const Text("Save"),
                        color: Colors.red,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.black)),
                      ),
                    )
                  ],
                ).toList()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ));
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          drawer: PagesDrawer(
            caller: "Scheduler",
            data: data,
          ),
          floatingActionButton: _buildGroupSpeedDial(),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Colors.blueGrey, Colors.redAccent])),
              child: data.schedules == null
                  ? null
                  : Center(
                      child: new ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                            onTapDown: (TapDownDetails details) =>
                                _lastPoint = Utils.onTap(details),
                            child: Card(
                                child: ListTile(
                              leading: Image.asset("assets/calendar.png"),
                              title: Text(
                                  data.schedules!.elementAt(index).groupName),
                              subtitle: Text(
                                  data.schedules!.elementAt(index).toString()),
                              onTap: () => Utils.toastShow(
                                data.schedules!.elementAt(index).toString(),
                              ),
                              onLongPress: () =>
                                  _popupMenu(data.schedules!.elementAt(index)),
                            )));
                      },
                      itemCount: data.schedules!.length,
                    ))),
        ));
  }
}
