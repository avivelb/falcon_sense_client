import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:dio/dio.dart';

class Client {
  Dio? _client;
  BuildContext? context;
  //bool get isConnected => _isConnected;
  static final String authenticationError = "AUTH_ERR";
  static final String generalError = "ERR";
  static final String serverOK = "OK";
  static final String getGroups = "GETGRP";
  static final String getSensors = "GETSENS";
  static final String getInspect = "ISINSPCT";
  static final String setInspect = "SETINSPCT";
  static final String getArmDelay = "GETARMDELAY";
  static final String setArmDelay = "SETARMDELAY";
  static final String getAlarmDelay = "GETALARMDELAY";
  static final String setAlarmDelay = "SETALARMDELAY";
  static final String scheduleArm = "SCHDARM";
  static final String getScheduleDaily = "GETSCHDAY";
  static final String addScheduleDaily = "ADDSCHDAY";
  static final String deleteScheduleDaily = "DELSCHDAY";
  static final String getHumidity = "GETHUMID";
  static final String alarm = "ALARM";
  static final String disAlarm = "DISALARM";
  static final String getLogs = "GETLOGS";
  static final String serverAddr = "elbaz.familyds.net";
  static final int serverPort = 8085;
  static final int notifyPort = 2001;
  static final String url = "https://$serverAddr:$serverPort";
  Client(BuildContext context) {
    context = context;
    this._client = Dio(BaseOptions(
      baseUrl: url,
      headers: {
        "Accept": "application/json",
      },
    ));
  }
  Future<String> send(String message) async {
    var response = await this._client?.post("/server", data: {"Data": message});
    return response?.data;
  }
}
