class Strings {
  /* Region of SharedPreferences constants */
  static const String is_auth_cached = "key_is_auth_cached";
  static const String username_cache = "key_username_cache";
  static const String pass_hash_cache = "key_pass_hash_cache";
  static const String app_token_cache = "key_app_token_cache";
  static const String notification_token_cache = "key_notification_token_cache";
  static const String is_notification_enabled_cache =
      "key_is_notification_enabled_cache";
  static const String is_inspect_mode_cache = "key_is_inspect_mode_cache";
  static const String notifications_cache = "key_notifications_cache";
  static const String arm_delay_cache = "key_arm_delay_cache";
  static const String alarm_delay_cache = "key_alarm_delay_cache";
  static const String is_app_active_cache = "key_is_app_active_cache";
  static const String system_temperature_cache = "key_system_temperature";
  static const String system_humidity_cache = "key_system_humidity";
  static const String system_is_cooling_cache = "key_system_is_cooling";
  static const String logs_cache = "key_logs";
  static const String current_notification_id = "key_current_notification_id";
/* EndOf Region of SharedPreferences constants */
/* Region of Notification titles constants */
  static const String inspect_alarm_title = "Inspect Alarm";
  static const String realtime_alarm_title = "Realtime Alarm";
  static const String physical_detection_title = "Physical Detection";
  static const String wireless_detection_title = "Wireless Detection";
  static const String group_arm_title = "Group Arm";
  static const String group_disarm_title = "Group Disarm";
  static const String group_schedule_title = "Group Schedule";
}
