import 'package:falcon_sense/widget/old_user.dart';
import 'package:flutter/material.dart';

class NewUser extends StatefulWidget {
  @override
  _NewUserState createState() => _NewUserState();
}

class _NewUserState extends State<NewUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
        gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [Colors.blueGrey, Colors.redAccent])),
        child: Stack(
          children: <Widget>[
          Center(
            child: Text('Create a user using your Synology NAS',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 38,
                  fontWeight: FontWeight.w900,
                ))),
          Positioned(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: OldUser()))
          ],
        ),
    ));
  }
}