class Integers {
  /* Region of Notification priority constants */
  static const int PRIORITY_REAL_TIME = 10;
  static const int PRIORITY_INSPECT = 9;
  static const int PRIORITY_DEBUG = 2;

/* EndOf Region of Notification priority constants */
  static const int FIRST_NOTIFICATION_ID = 2;
  static const int MAX_NOTIFICATION_IN_SCREEN = 1000;
}
