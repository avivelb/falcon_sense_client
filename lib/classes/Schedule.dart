class Schedule {
  String groupName;
  String day;
  String time;
  String action;

  Schedule(this.groupName, this.day, this.time, this.action);

  factory Schedule.fromJson(dynamic jsonData) {
    return Schedule(jsonData['Group_Name'], jsonData['Day'], jsonData['Time'],
        jsonData['Action']);
  }

  @override
  String toString() {
    return "Schedule{Group_Name:${this.groupName},Day:${this.day},Time:${this.time},Action:${this.action}}";
  }
}
