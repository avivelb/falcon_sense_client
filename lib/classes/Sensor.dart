class Sensor {
  String name;
  int gpIO;
  int pin;
  int length;
  String location;
  String type;

  Sensor(this.name, this.gpIO, this.pin, this.length, this.location,
      this.type);

  factory Sensor.fromJson(dynamic jsonData) {
    return Sensor(
        jsonData['Name'],
        jsonData['GPIO'] as int,
        jsonData['Pin'] as int,
        jsonData['Wireless_Length'] as int,
        jsonData['Location'],
        jsonData['Sensor_Type']);
  }

  @override
  String toString() {
    return 'Sensor{name: $name, GPIO: $gpIO, pin: $pin, length: $length, location: $location, type: $type}';
  }

}
