import 'package:flutter/material.dart';

class LoginButton extends StatefulWidget {
  final GestureTapCallback onPressed;
  const LoginButton({Key? key, required this.onPressed}) : super(key: key);
  @override
  _LoginButtonState createState() => _LoginButtonState(onPressed: onPressed);
}

class _LoginButtonState extends State<LoginButton> {
  _LoginButtonState({required this.onPressed});
  final GestureTapCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 0, right: 0, left: 50),
        child: Container(
          alignment: Alignment.bottomRight,
          height: 50,
          width: 100,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.red[300]!,
                blurRadius: 10.0, // has the effect of softening the shadow
                spreadRadius: 1.0, // has the effect of extending the shadow
                offset: const Offset(
                  5.0, // horizontal, move right 10
                  5.0, // vertical, move down 10
                ),
              ),
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(30),
          ),
          child: FlatButton(
            onPressed: onPressed,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'OK',
                  style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const Icon(
                  Icons.arrow_forward,
                  color: Colors.red,
                ),
              ],
            ),
          ),
        ));
  }
}
