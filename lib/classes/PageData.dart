import 'dart:core';
import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/Schedule.dart';
import 'package:falcon_sense/classes/Sensor.dart';

class PageData {
  String? token;
  Client? client;
  List<Sensor>? sensors;
  List<Group>? groups;
  List<Schedule>? schedules;
  PageData(
      {required this.token,
      required this.client,
      required this.sensors,
      required this.groups,
      required this.schedules});
}
