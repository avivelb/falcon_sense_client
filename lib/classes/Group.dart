import 'package:falcon_sense/classes/Sensor.dart';

class Group {
  String? name;
  List<Sensor>? sensors;
  bool isTriggered;
  bool isScheduledArm;
  bool isScheduledDisarm;
  bool isAlarming;
  Group(this.name, this.sensors, this.isTriggered, this.isScheduledArm,
      this.isScheduledDisarm, this.isAlarming);
  factory Group.fromJson(dynamic jsonData, Map<String, Sensor> sensorMap) {
    List<Sensor> feed = [];
    List names = jsonData['Data'];
    names.forEach((name) {
      feed.add(sensorMap[name]!);
    });
    return Group(
      jsonData['Name'],
      feed,
      jsonData['Is_Triggered'] as bool,
      jsonData['Is_Scheduled_Arm'] as bool,
      jsonData['Is_Scheduled_Disarm'] as bool,
      jsonData['Is_Alarming'] as bool,
    );
  }
  @override
  String toString() {
    String buffer = "Triggered: ${this.isTriggered}\n";
    buffer += "Scheduled to arm: ${this.isScheduledArm}\n";
    buffer += "Scheduled to disarm: ${this.isScheduledDisarm}\n";
    buffer += "Is Alarming: ${this.isAlarming}\n";
    this.sensors!.forEach((element) {
      if (element.type == "Physical") {
        buffer += "${element.name} on GPIO ${element.gpIO}\n";
      } else if (element.type == "Wireless") {
        buffer += "${element.name} with length ${element.length}\n";
      }
    });
    return buffer;
  }
}
