import 'package:falcon_sense/classes/BasicComponents.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:falcon_sense/widget/horizontal_text.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:falcon_sense/widget/thermometer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Thermostat extends StatefulWidget {
  final PageData data;
  const Thermostat({Key? key, required this.data}) : super(key: key);
  @override
  _ThermostatState createState() => _ThermostatState(data: this.data);
}

class _ThermostatState extends State<Thermostat> {
  _ThermostatState({required this.data});
  final PageData data;
  SpeedDial _buildThermostatSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.refresh, color: Colors.white),
          backgroundColor: Colors.red,
          onTap: () {
            BasicComponents(data).refreshHumidity();
            Navigator.pop(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Thermostat(
                          data: data,
                        )));
          },
          label: 'Refresh',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.red,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
            drawer: PagesDrawer(
              caller: "Thermostat",
              data: data,
            ),
            floatingActionButton: _buildThermostatSpeedDial(),
            body: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.blueGrey, Colors.redAccent])),
                child: Column(
                  children: <Widget>[
                    HorizontalText(
                      text:
                          "Humidity: ${sharedPrefs.humidity!.toStringAsFixed(2)} %",
                      top: 60,
                      left: 10,
                    ),
                    HorizontalText(
                      text:
                          "Temperature: ${sharedPrefs.temperature!.toStringAsFixed(2)} C",
                      top: 60,
                      left: 10,
                    ),
                    HorizontalText(
                      text: "Cooling fans state:",
                      top: 60,
                      left: 10,
                    ),
                    Transform.scale(
                        scale: 2.0,
                        child: Switch(
                          activeColor: Colors.red,
                          value: sharedPrefs.isCooling!,
                          onChanged: (value) => {},
                        )),
                    Thermometer(),
                  ],
                ))));
  }
}
