import 'package:flutter/material.dart';

class HorizontalText extends StatefulWidget {
  final String text;
  final double top;
  final double left;
  const HorizontalText(
      {Key? key, required this.text, required this.top, required this.left})
      : super(key: key);
  @override
  _HorizontalTextState createState() =>
      _HorizontalTextState(this.text, this.top, this.left);
}

class _HorizontalTextState extends State<HorizontalText> {
  final String text;
  final double top;
  final double left;
  _HorizontalTextState(this.text, this.top, this.left);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: top, left: left),
      child: Text(
        this.text,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.w900,
        ),
      ),
    );
  }
}
