import 'package:falcon_sense/classes/StringReference.dart';
import 'package:flutter/material.dart';

class CustomDropDown extends StatefulWidget {
  final String hint;
  final List<String> choices;
  final StringReference choice;
  const CustomDropDown(
      {Key? key,
      required this.hint,
      required this.choices,
      required this.choice})
      : super(key: key);
  @override
  _CustomDropDownState createState() => _CustomDropDownState(
      hint: this.hint, choices: this.choices, choice: this.choice);
}

class _CustomDropDownState extends State<CustomDropDown> {
  _CustomDropDownState(
      {required this.hint, required this.choices, required this.choice});
  final String hint;
  final List<String> choices;
  StringReference choice;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 300,
        padding: EdgeInsets.all(5),
        child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
                isExpanded: true,
                items: this.choices.map((String val) {
                  return DropdownMenuItem<String>(
                    value: val,
                    child: Text(val),
                  );
                }).toList(),
                hint: Text(this.hint),
                value: this.choice.str,
                onChanged: (String? val) {
                  setState(() => this.choice.str = val!);
                })));
  }
}
