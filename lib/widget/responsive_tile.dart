import 'package:falcon_sense/classes/StringReference.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class ResponsiveTimePicker extends StatefulWidget {
  final String title;
  final StringReference selectedTimeFormat;
  const ResponsiveTimePicker(
      {Key? key, required this.title, required this.selectedTimeFormat})
      : super(key: key);
  @override
  _ResponsiveTimePickerState createState() => _ResponsiveTimePickerState(
      title: title, selectedTimeFormat: selectedTimeFormat);
}

class _ResponsiveTimePickerState extends State<ResponsiveTimePicker> {
  _ResponsiveTimePickerState(
      {required this.title, required this.selectedTimeFormat});
  final String title;
  final StringReference selectedTimeFormat;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Listener(
        onPointerUp: _timePicker,
        child: SettingsTile(title: title, subtitle: selectedTimeFormat.str),
      ),
    );
  }

  void _timePicker(PointerEvent details) {
    showTimePicker(
      initialTime: TimeOfDay.now(),
      context: context,
    ).then((result) {
      setState(() {
        selectedTimeFormat.str = result == null
            ? "None"
            : "${result.hour.toString().padLeft(2, '0')}:${result.minute.toString().padLeft(2, '0')}";
      });
    });
  }
}
