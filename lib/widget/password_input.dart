import 'package:flutter/material.dart';

class PasswordInput extends StatefulWidget {
  final TextEditingController passwordController;
  const PasswordInput({Key? key, required this.passwordController})
      : super(key: key);
  @override
  _PasswordInputState createState() => _PasswordInputState(passwordController);
}

class _PasswordInputState extends State<PasswordInput> {
  final TextEditingController passwordController;
  _PasswordInputState(this.passwordController);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        child: TextField(
          controller: passwordController,
          style: TextStyle(
            color: Colors.white,
          ),
          obscureText: true,
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: 'Password',
            labelStyle: TextStyle(
              color: Colors.white70,
            ),
          ),
        ),
      ),
    );
  }
}
