import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsScreen extends StatefulWidget {
  final PageData data;
  const SettingsScreen({Key? key, required this.data}) : super(key: key);
  @override
  _SettingsScreenState createState() => _SettingsScreenState(data: data);
}

class _SettingsScreenState extends State<SettingsScreen> {
  final PageData data;
  Point? _lastPoint;
  //bool lockInBackground = true;
  _SettingsScreenState({required this.data});
  @override
  void initState() {
    super.initState();
    if (SchedulerBinding.instance!.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance?.addPostFrameCallback((_) {
        _getComponents();
      });
    }
  }

  Future<void> _getComponents() async {
    await _checkInspectMode();
    await _getArmDelay();
    await _getAlarmDelay();
  }

  Future<void> _checkInspectMode() async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.getInspect}");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow(
          "Cannot retrieve Inspect mode details, fatal connection error!");
      Navigator.pop(context);
    }
    setState(() {
      sharedPrefs.isInspectMode = (responseSpl?[1] == "True");
    });
  }

  void _updateInspectMode(bool value) async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.setInspect}#$value");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow("Cannot change Inspect mode, fatal connection error!");
      return;
    }
    if (responseSpl?[1] != Client.serverOK) {
      Utils.toastShow("Server didn't approve the inspect mode change!");
      return;
    }
    setState(() {
      sharedPrefs.isInspectMode = value;
    });
  }

  Future<void> _getArmDelay() async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.getArmDelay}");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow(
        "Cannot retrieve arm delay details, fatal connection error!",
      );
      Navigator.pop(context);
    }
    setState(() {
      sharedPrefs.armDelay = int.parse(responseSpl![1]);
    });
  }

  void _updateArmDelay(int value) async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.setArmDelay}#$value");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow("Cannot change arm delay, fatal connection error!");
      return;
    }
    if (responseSpl?[1] != Client.serverOK) {
      Utils.toastShow("Server didn't approve the arm delay change!");
      return;
    }
    setState(() {
      sharedPrefs.armDelay = value;
    });
  }

  Future<void> _getAlarmDelay() async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.getAlarmDelay}");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow(
        "Cannot retrieve alarm delay details, fatal connection error!",
      );
      Navigator.pop(context);
    }
    setState(() {
      sharedPrefs.alarmDelay = int.parse(responseSpl![1]);
    });
  }

  void _updateAlarmDelay(int value) async {
    Utils.showLoaderDialog(context);
    var response =
        await data.client?.send("${data.token}#${Client.setAlarmDelay}#$value");
    Navigator.pop(context);
    var responseSpl = response?.split("#");
    if (responseSpl?.length != 2) {
      Utils.toastShow("Cannot change arm delay, fatal connection error!");
      return;
    }
    if (responseSpl![1] != Client.serverOK) {
      Utils.toastShow("Server didn't approve the alarm delay change!");
      return;
    }
    setState(() {
      sharedPrefs.alarmDelay = value;
    });
  }

  _changeArmDelayPopupMenu() async {
    int? _value;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("30 Seconds"),
          value: 30,
        ),
        PopupMenuItem(
          child: Text("40 Seconds"),
          value: 40,
        ),
        PopupMenuItem(
          child: Text("60 Seconds"),
          value: 60,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value!);
    if (_value != sharedPrefs.armDelay) {
      _updateArmDelay(_value!);
    }
  }

  _changeAlarmDelayPopupMenu() async {
    int? _value;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("50 Seconds"),
          value: 50,
        ),
        PopupMenuItem(
          child: Text("60 Seconds"),
          value: 60,
        ),
        PopupMenuItem(
          child: Text("70 Seconds"),
          value: 70,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value!);
    if (_value != sharedPrefs.alarmDelay) {
      _updateAlarmDelay(_value!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Settings'),
          backgroundColor: Colors.redAccent,
        ),
        body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.blueGrey, Colors.redAccent])),
            child: GestureDetector(
              onTapDown: (TapDownDetails details) =>
                  _lastPoint = Utils.onTap(details),
              child: SettingsList(
                sections: [
                  SettingsSection(
                    title: 'Common',
                    // titleTextStyle: TextStyle(fontSize: 30),
                    tiles: [
                      SettingsTile(
                        title: 'Language',
                        subtitle: 'English',
                        leading: Icon(Icons.language),
                        onTap: () {},
                      ),
                      SettingsTile(
                        title: 'Environment',
                        subtitle: 'Production',
                        leading: Icon(Icons.cloud_queue),
                        onTap: () => print('e'),
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Account',
                    tiles: [
                      SettingsTile(
                          title: 'Phone number', leading: Icon(Icons.phone)),
                      SettingsTile(title: 'Email', leading: Icon(Icons.email)),
                      SettingsTile(
                        title: 'Sign out',
                        leading: Icon(Icons.exit_to_app),
                        onTap: () => Navigator.popUntil(context,
                            ModalRoute.withName(Navigator.defaultRouteName)),
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Security',
                    tiles: [
                      SettingsTile.switchTile(
                        title: 'Save login details in cache after login',
                        leading: Icon(Icons.account_circle),
                        switchValue: sharedPrefs.cached,
                        switchActiveColor: Colors.red,
                        onToggle: (bool value) {
                          setState(() {
                            sharedPrefs.cached = value;
                          });
                        },
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Notifications',
                    tiles: [
                      SettingsTile.switchTile(
                        title: 'Enable Notifications',
                        leading: Icon(Icons.notifications_active),
                        switchValue: sharedPrefs.isNotificationEnabled,
                        switchActiveColor: Colors.red,
                        onToggle: (value) {
                          setState(() {
                            sharedPrefs.isNotificationEnabled = value;
                          });
                        },
                      ),
                      SettingsTile.switchTile(
                        title: 'Inspect Mode',
                        leading: Icon(Icons.notifications_none),
                        switchValue: sharedPrefs.isInspectMode,
                        switchActiveColor: Colors.red,
                        onToggle: (value) => _updateInspectMode(value),
                      ),
                    ],
                  ),
                  SettingsSection(
                    title: 'Arm & Alarm',
                    tiles: [
                      SettingsTile(
                        title: 'Arm Delay',
                        subtitle: "${sharedPrefs.armDelay.toString()} Seconds",
                        onTap: _changeArmDelayPopupMenu,
                      ),
                      SettingsTile(
                        title: 'Alarm Delay',
                        subtitle:
                            "${sharedPrefs.alarmDelay.toString()} Seconds",
                        onTap: _changeAlarmDelayPopupMenu,
                      )
                    ],
                  ),
                  SettingsSection(
                    title: 'Misc',
                    tiles: [
                      SettingsTile(
                          title: 'Terms of Service',
                          leading: Icon(Icons.description)),
                      SettingsTile(
                          title: 'Open source licenses',
                          leading: Icon(Icons.collections_bookmark)),
                    ],
                  ),
                  CustomSection(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 22, bottom: 8),
                        ),
                        Text(
                          'Version: 0.1.1 (Alpha)',
                          style: TextStyle(color: Color(0xFF777777)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }
}
