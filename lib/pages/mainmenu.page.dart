import 'dart:ui';
import 'package:falcon_sense/classes/BasicComponents.dart';
import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:falcon_sense/services/notifications_main.dart';

class MainMenu extends StatefulWidget {
  final PageData data;
  const MainMenu({Key? key, required this.data}) : super(key: key);
  @override
  _MainMenuState createState() => _MainMenuState(
        data: this.data,
      );
}

class _MainMenuState extends State<MainMenu> {
  _MainMenuState({required this.data});
  final PageData data;
  Point? _lastPoint;
  String? _enteredCode;
  @override
  void initState() {
    var channel = const MethodChannel('xyz.elbaz/background_service');
    var callbackHandle = PluginUtilities.getCallbackHandle(backgroundMain);
    channel.invokeMethod('startService', callbackHandle!.toRawHandle());
    super.initState();
  }

  void _toggleGroup(Group group) async {
    String toSend = "${data.token}#TGLGRP#${group.name}";
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      setState(() {
        group.isTriggered = !group.isTriggered;
      });
    } else {
      if (resultPart != null) {
        Utils.toastShow("An Error occurred: $resultPart");
      } else {
        Utils.toastShow("An Error occurred");
      }
    }
  }

  void _scheduleGroup(Group group) async {
    Utils.showLoaderDialog(context);
    String response = await data.client!
        .send("${data.token}#${Client.scheduleArm}#${group.name}");
    Navigator.pop(context);
    String responsePart = response.split("#")[1];
    if (responsePart != Client.serverOK) {
      Utils.toastShow("Schedule alarm failure: $responsePart");
      return;
    }
    setState(() {
      group.isScheduledArm = true;
    });
  }

  _mainMenuPopUp(Group group) async {
    int? _value;
    const int toggle = 1;
    const int scheduleGroupArm = 2;
    const int viewGroup = 3;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("Toggle Group"),
          value: toggle,
        ),
        PopupMenuItem(
          child: Text("Schedule Group to arm"),
          value: scheduleGroupArm,
        ),
        PopupMenuItem(
          child: Text("View Group"),
          value: viewGroup,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value);
    switch (_value) {
      case toggle:
        await Utils.askDialog(
            context, "Group Toggle", "Are you sure?", _toggleGroup, group);
        break;
      case scheduleGroupArm:
        await Utils.askDialog(
            context, "Group Arm", "Are you sure?", _scheduleGroup, group);
        break;
      case viewGroup:
        Utils.viewGroupDialog(group, context);
        break;
    }
  }

  SpeedDial _buildMainSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.refresh, color: Colors.white),
          backgroundColor: Colors.blue,
          onTap: () {
            BasicComponents(data).refreshComponents();
            Navigator.pop(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MainMenu(
                          data: data,
                        )));
          },
          label: 'Refresh',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.blue,
        ),
        SpeedDialChild(
          child: Icon(Icons.alarm_on, color: Colors.white),
          backgroundColor: Colors.green,
          onTap: () async {
            await _alarmCodeInput(context);
            Utils.showLoaderDialog(context);
            String response = await data.client!
                .send("${data.token}#${Client.alarm}#$_enteredCode");
            Navigator.pop(context);
            String responsePart = response.split("#")[1];
            if (responsePart != Client.serverOK) {
              Utils.toastShow("Alarm Failure!");
              return;
            }
          },
          label: 'Alarm',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.green,
        ),
        SpeedDialChild(
          child: Icon(Icons.alarm_off, color: Colors.white),
          backgroundColor: Colors.red,
          onTap: () async {
            await _alarmCodeInput(context);
            Utils.showLoaderDialog(context);
            String response = await data.client!
                .send("${data.token}#${Client.disAlarm}#$_enteredCode");
            Navigator.pop(context);
            String responsePart = response.split("#")[1];
            if (responsePart != Client.serverOK) {
              Utils.toastShow("Disable Alarm Failure!");
              return;
            }
          },
          label: 'Disable Alarm',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.red,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          drawer: PagesDrawer(
            caller: "Main Menu",
            data: data,
          ),
          floatingActionButton: _buildMainSpeedDial(),
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blueGrey, Colors.redAccent]),
            ),
            child: GridView.builder(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemBuilder: (context, index) {
                return GestureDetector(
                    onTapDown: (TapDownDetails details) =>
                        _lastPoint = Utils.onTap(details),
                    child: ButtonTheme(
                        buttonColor: (data.groups!.elementAt(index).isTriggered)
                            ? Colors.green
                            : data.groups!.elementAt(index).isScheduledArm
                                ? Colors.yellow
                                : Colors.red,
                        child: RaisedButton(
                          onLongPress: () =>
                              _mainMenuPopUp(data.groups!.elementAt(index)),
                          onPressed: () {},
                          child: Text(data.groups!.elementAt(index).name!,
                              style: Theme.of(context).textTheme.headline5),
                        )));
              },
              itemCount: data.groups!.length,
            ),
          ),
        ));
  }

  Future<void> _alarmCodeInput(BuildContext context) async {
    TextEditingController controller = new TextEditingController();
    _enteredCode = "";
    String? valueText;
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Enter Alarm code'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: controller,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: "Code"),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  setState(() {
                    _enteredCode = valueText;
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
}
