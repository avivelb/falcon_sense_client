import 'dart:convert';
import 'package:falcon_sense/pages/login.page.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:falcon_sense/classes/IntegerReference.dart';
import 'package:falcon_sense/classes/integers.dart';
import 'package:falcon_sense/classes/strings.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/pages/logs.page.dart';
import 'package:flutter/material.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:falcon_sense/classes/Client.dart';
import 'package:web_socket_channel/io.dart';

void backgroundMain() async {
  WidgetsFlutterBinding.ensureInitialized();
  await sharedPrefs.init();
  final String listenUrl =
      "wss://${Client.serverAddr}:${Client.notifyPort}/stream";
  IntegerReference _notificationId =
      new IntegerReference(Integers.FIRST_NOTIFICATION_ID);
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  initNotifications(flutterLocalNotificationsPlugin);
  var channel = IOWebSocketChannel.connect(listenUrl, headers: {
    "X-Gotify-Key": sharedPrefs.notificationToken!,
    "accept": "application/json"
  });
  connect() async {
    if (channel != null) {
      // add in a reconnect delay
      await Future.delayed(Duration(seconds: 4));
    }
    wserror(err) async {
      debugPrint(new DateTime.now().toString() + " Connection error: $err");
      await connect();
    }

    debugPrint(
        new DateTime.now().toString() + " Starting connection attempt...");
    channel = IOWebSocketChannel.connect(listenUrl, headers: {
      "X-Gotify-Key": sharedPrefs.notificationToken!,
      "accept": "application/json"
    });
    getLatestNotifications(flutterLocalNotificationsPlugin, _notificationId);
    debugPrint(
        new DateTime.now().toString() + " Connection attempt completed.");
    channel.stream.listen((msg) {
      messageHandler(
          flutterLocalNotificationsPlugin, jsonDecode(msg), _notificationId);
      sharedPrefs.reload();
    }, onDone: () {
      connect();
    }, onError: wserror, cancelOnError: true);
  }

  connect();
}

void getLatestNotifications(
    flutterLocalNotificationsPlugin, localNotificationID) async {
  Dio client = Dio(BaseOptions(
    baseUrl: "https://${Client.serverAddr}:${Client.notifyPort}",
    headers: {
      "Accept": "application/json",
      "X-Gotify-Key": sharedPrefs.notificationToken,
    },
  ));
  var response = await client.get(
      "/message?limit=100&since=${sharedPrefs.currentNotificationID! + 100}");
  var latestMessages = response.data["messages"] as List;
  int maxID = sharedPrefs.currentNotificationID!;
  latestMessages.forEach((msg) {
    if (msg["id"] > sharedPrefs.currentNotificationID!) {
      showNotification(msg["title"], "${msg["message"]} on ${msg["date"]}",
          flutterLocalNotificationsPlugin, localNotificationID);
    }
    if (msg["id"] > maxID) {
      maxID = msg["id"];
    }
  });
  sharedPrefs.currentNotificationID = maxID;
}

void messageHandler(flutterLocalNotificationsPlugin, jsonMessage,
    IntegerReference notificationId) async {
  // Add , bool notify_aloud
  sharedPrefs.currentNotificationID = jsonMessage["id"];
  if (sharedPrefs.isAppActive! &&
      sharedPrefs.isInspectMode! &&
      jsonMessage["priority"] == Integers.PRIORITY_INSPECT) {
    if (jsonMessage["title"] == Strings.inspect_alarm_title) {
      await Utils.playLocalAsset("inspect_mode_alarm.mp3");
    }
  } else if (!sharedPrefs.isInspectMode! &&
      jsonMessage["priority"] == Integers.PRIORITY_REAL_TIME) {
    if (jsonMessage["title"] == Strings.realtime_alarm_title) {
      await Utils.playLocalAsset("alarm_start.mp3");
    }
  }
  if (sharedPrefs.isNotificationEnabled! ||
      (sharedPrefs.isAppActive! && sharedPrefs.isInspectMode!)) {
    switch (jsonMessage["title"]) {
      case Strings.physical_detection_title:
        await Utils.playLocalAsset("physical_sensors_detection.mp3");
        break;
      case Strings.wireless_detection_title:
        await Utils.playLocalAsset("wireless_sensors_detection.mp3");
        break;
      case Strings.group_arm_title:
        await Utils.playLocalAsset("sensors_armed.mp3");
        break;
      case Strings.group_disarm_title:
        await Utils.playLocalAsset("sensors_disarmed.mp3");
        break;
      case Strings.group_schedule_title:
        await Utils.playLocalAsset("sensors_scheduled.mp3");
        break;
    }
  }
  showNotification(jsonMessage["title"], jsonMessage["message"],
      flutterLocalNotificationsPlugin, notificationId);
}

Future<void> onSelectNotification(String payload) async {}
void initNotifications(flutterLocalNotificationsPlugin) {
  var initializationSettingsAndroid =
      AndroidInitializationSettings('mipmap/falcon_launcher');
  var initializationSettingsIOs = IOSInitializationSettings();
  var initSetttings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOs);
  flutterLocalNotificationsPlugin.initialize(initSetttings);
}

void showNotification(
    String title,
    String content,
    flutterLocalNotificationsPlugin,
    IntegerReference currentNotificationId) async {
  var android = AndroidNotificationDetails('id', 'channel ', 'description',
      priority: Priority.high, importance: Importance.max);
  var iOS = IOSNotificationDetails();
  var platform = new NotificationDetails(android: android, iOS: iOS);
  await flutterLocalNotificationsPlugin.show(
      currentNotificationId.value++, title, content, platform,
      payload: "f");
  // Max notifications to appear in screen is Integers.MAX_NOTIFICATION_IN_SCREEN
  currentNotificationId.value =
      (currentNotificationId.value >= Integers.MAX_NOTIFICATION_IN_SCREEN)
          ? Integers.FIRST_NOTIFICATION_ID
          : currentNotificationId.value;
}
