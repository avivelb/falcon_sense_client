import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:falcon_sense/classes/Sensor.dart';
import 'package:falcon_sense/classes/StringReference.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/widget/group_creator.dart';
import 'package:falcon_sense/widget/group_editor.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:collection/collection.dart';

class GroupMenu extends StatefulWidget {
  final PageData data;
  const GroupMenu({Key? key, required this.data}) : super(key: key);
  @override
  _GroupMenuState createState() => _GroupMenuState(data: this.data);
}

class _GroupMenuState extends State<GroupMenu> {
  _GroupMenuState({required this.data});
  final PageData data;
  Point? _lastPoint;
  List<Sensor>? _newSensorsPick;
  List<Sensor>? _addGroupSensors;
  StringReference? _addGroupName;
  TextEditingController _addSensorGroupController = new TextEditingController();
  SpeedDial _buildGroupSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.add, color: Colors.white),
          backgroundColor: Colors.red,
          onTap: () => _addGroupDialog(),
          label: 'Add group',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.red,
        ),
      ],
    );
  }

  _popupMenu(Group group) async {
    int? _value;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_lastPoint!.x, _lastPoint!.y, 100, 400),
      items: [
        PopupMenuItem(
          child: Text("View"),
          value: 1,
        ),
        PopupMenuItem(
          child: Text("Edit"),
          value: 2,
        ),
        PopupMenuItem(
          child: Text("Toggle Group"),
          value: 3,
        ),
        PopupMenuItem(
          child: Text("Delete"),
          value: 4,
        ),
      ],
      elevation: 8.0,
    ).then((value) => _value = value);
    switch (_value) {
      case 1:
        Utils.viewGroupDialog(group, context);
        break;
      // Edit Group sensors
      case 2:
        _editGroupDialog(group);
        break;
      case 3:
        _toggleGroupDialog(group);
        break;
      case 4:
        _deleteGroupDialog(group);
        break;
    }
  }

  void _addGroupDialog() async {
    _addGroupName = new StringReference("");
    _addGroupSensors = [];
    _addSensorGroupController.text = "";
    showGeneralDialog(
      barrierLabel: "Add Group",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 450,
              child: GroupCreator(
                allSensors: data.sensors!,
                name: new StringReference(""),
                selectedSensors: this._addGroupSensors!,
                onSaveFunction: () => _addGroup(),
                controller: this._addSensorGroupController,
              ),
              margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  void _toggleGroupDialog(Group group) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        _toggleGroup(group);
        Navigator.of(_customContext!).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Group Toggle"),
      content: Text("Are you sure you want to toggle the selected group?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  void _toggleGroup(Group group) async {
    String toSend = "${data.token}#TGLGRP#${group.name}";
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      setState(() {
        group.isTriggered = !group.isTriggered;
      });
    } else {
      if (resultPart != null) {
        Utils.toastShow("An Error occurred: $resultPart");
      } else {
        Utils.toastShow("An Error occurred");
      }
    }
  }

  void _deleteGroupDialog(Group group) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        _deleteGroup(group);
        if (_customContext != null) {
          Navigator.of(_customContext!).pop();
        }
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Group Deletion"),
      content: Text("Are you sure you want to delete the selected group?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  void _deleteGroup(Group group) async {
    String toSend = "${data.token}#DELGRP#${group.name}";
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      setState(() {
        data.groups?.remove(group);
      });
    } else {
      if (resultPart != null) {
        Utils.toastShow("An Error occurred: $resultPart");
      } else {
        Utils.toastShow("An Error occurred");
      }
    }
  }

  void _addGroup() async {
    Group? newGroup = new Group(null, null, false, false, false, false);
    if (this._addSensorGroupController.text.isEmpty) {
      Utils.toastShow("Please give the new group a name!");
      return;
    }
    if (this._addGroupSensors!.length < 1) {
      Utils.toastShow("Please give at least one sensor to the new group");
      return;
    }
    String toSend =
        "${data.token}#ADDGRP#${_addSensorGroupController.text}#${_sensorSplitRepr(_addGroupSensors!)}";
    Utils.showLoaderDialog(context);
    String response = await data.client!.send(toSend);
    Navigator.pop(context);
    String resultPart = response.split("#")[1];
    if (resultPart == "OK") {
      newGroup.name = _addSensorGroupController.text;
      newGroup.sensors = _addGroupSensors!;
      setState(() {
        data.groups?.add(newGroup);
      });
    } else {
      if (resultPart != null) {
        Utils.toastShow("An Error occurred: $resultPart");
      } else {
        Utils.toastShow("An Error occurred");
      }
    }
  }

  void _editGroupDialog(Group group) async {
    _newSensorsPick = new List<Sensor>.from(group.sensors!);
    showGeneralDialog(
      barrierLabel: "Edit Group",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 450,
              child: GroupEditor(
                allSensors: data.sensors!,
                selectedSensors: this._newSensorsPick!,
                onSaveFunction: () => _editGroup(group),
              ),
              margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  String _sensorSplitRepr(List<Sensor> sensors) {
    String buffer = "";
    sensors.forEach((sensor) {
      buffer += "${sensor.name},";
    });
    return buffer.substring(0, buffer.length - 1);
  }

  void _editGroup(Group group) async {
    Function equality = const DeepCollectionEquality.unordered().equals;
    if (!equality(group.sensors, _newSensorsPick)) {
      if (_newSensorsPick!.length >= 1) {
        Utils.showLoaderDialog(context);
        String response = await data.client!.send(
            "${data.token}#EDITGRPSEN#${group.name}#${_sensorSplitRepr(_newSensorsPick!)}");
        Navigator.pop(context);
        String resultPart = response.split("#")[1];
        if (resultPart == "OK") {
          setState(() {
            group.sensors = _newSensorsPick;
          });
        } else {
          Utils.toastShow("An Error occurred");
        }
      } else {
        Utils.toastShow("Please select at least one sensor!");
      }
    } else {
      Utils.toastShow("No changes were made to the group!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          drawer: PagesDrawer(
            caller: "Sensor Groups",
            data: data,
          ),
          floatingActionButton: _buildGroupSpeedDial(),
          body: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Colors.blueGrey, Colors.redAccent])),
              child: data.groups == null
                  ? null
                  : Center(
                      child: new ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                            onTapDown: (TapDownDetails details) =>
                                _lastPoint = Utils.onTap(details),
                            child: Card(
                                child: ListTile(
                              leading: Image.asset("assets/group.png"),
                              title: Text(data.groups!.elementAt(index).name!),
                              subtitle: Text(
                                  data.groups!.elementAt(index).toString()),
                              onTap: () => Utils.toastShow(
                                data.groups!.elementAt(index).toString(),
                              ),
                              onLongPress: () =>
                                  _popupMenu(data.groups!.elementAt(index)),
                            )));
                      },
                      itemCount: data.groups!.length,
                    ))),
        ));
  }
}
