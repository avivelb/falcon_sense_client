class Point {
  double _x;
  double _y;

  Point(this._x, this._y);

  double get x => _x;

  double get y => _y;
}
