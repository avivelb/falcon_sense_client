import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/pages/groupmenu.page.dart';
import 'package:falcon_sense/pages/logs.page.dart';
import 'package:falcon_sense/pages/mainmenu.page.dart';
import 'package:falcon_sense/pages/scheduler.page.dart';
import 'package:falcon_sense/pages/sensormenu.page.dart';
import 'package:falcon_sense/pages/settings.page.dart';
import 'package:falcon_sense/pages/thermostat.page.dart';
import 'package:flutter/material.dart';

class PagesDrawer extends StatefulWidget {
  final String caller;
  final PageData data;
  const PagesDrawer({Key? key, required this.caller, required this.data})
      : super(key: key);
  @override
  _PagesDrawerState createState() =>
      _PagesDrawerState(caller: this.caller, data: this.data);
}

class _PagesDrawerState extends State<PagesDrawer> {
  _PagesDrawerState({required this.caller, required this.data});
  final String caller;
  final PageData data;
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.blueGrey, Colors.redAccent])),
            child: Center(
                child: Text(
              'Falcon Sense',
              style: TextStyle(
                color: Colors.black,
                fontSize: 24,
              ),
            )),
          ),
          ListTile(
              title: Text('Sensors'),
              onTap: () {
                if (this.caller != "Sensors")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SensorMenu(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Sensor Groups'),
              onTap: () {
                if (this.caller != "Sensor Groups")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GroupMenu(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Main Menu'),
              onTap: () {
                if (this.caller != "Main Menu")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MainMenu(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Scheduler'),
              onTap: () {
                if (this.caller != "Scheduler")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Scheduler(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Thermostat'),
              onTap: () {
                if (this.caller != "Thermostat")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Thermostat(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Settings'),
              onTap: () {
                if (this.caller != "Settings")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SettingsScreen(
                                data: data,
                              )));
              }),
          ListTile(
              title: Text('Logs'),
              onTap: () {
                if (this.caller != "Logs")
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Logs(
                                data: data,
                              )));
              }),
        ],
      ),
    );
  }
}
