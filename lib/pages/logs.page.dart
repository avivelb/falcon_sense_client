import 'package:falcon_sense/classes/BasicComponents.dart';
import 'package:falcon_sense/classes/Client.dart';
import 'package:falcon_sense/classes/PageData.dart';
import 'package:falcon_sense/classes/SharedPrefs.dart';
import 'package:falcon_sense/classes/utils.dart';
import 'package:falcon_sense/widget/horizontal_text.dart';
import 'package:falcon_sense/widget/pages_drawer.dart';
import 'package:falcon_sense/widget/thermometer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Logs extends StatefulWidget {
  final PageData data;
  const Logs({Key? key, required this.data}) : super(key: key);
  @override
  _LogsState createState() => _LogsState(data: this.data);
}

class _LogsState extends State<Logs> {
  _LogsState({required this.data});
  final PageData data;
  String? logs;
  List<String> l = [];
  @override
  void initState() {
    super.initState();
    if (SchedulerBinding.instance?.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance?.addPostFrameCallback((_) {
        _getComponents();
      });
    }
  }

  Future<void> _getComponents() async {
    _getLogs();
  }

  Future<void> _getLogs() async {
    Utils.showLoaderDialog(context);
    logs = await data.client!.send("${data.token}#${Client.getLogs}");
    Navigator.pop(context);
    var logSpl = logs?.split("\n");
    setState(() {
      logSpl?.forEach((element) {
        l.add(element);
      });
      l = l.reversed.toList();
    });
  }

  SpeedDial _buildLogsSpeedDial() {
    return SpeedDial(
      backgroundColor: Colors.black,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      curve: Curves.bounceIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.refresh, color: Colors.white),
          backgroundColor: Colors.blue,
          onTap: () {
            _getLogs();
          },
          label: 'Refresh',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.blue,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          floatingActionButton: _buildLogsSpeedDial(),
          body: ListView.builder(
            itemCount: l.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(l[index]),
              );
            },
          ),
          drawer: PagesDrawer(
            caller: "Logs",
            data: data,
          ),
        ));
  }
}
