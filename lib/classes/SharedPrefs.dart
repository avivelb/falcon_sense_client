import 'package:falcon_sense/classes/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static SharedPreferences? _sharedPrefs;
  init() async {
    if (_sharedPrefs == null) {
      _sharedPrefs = await SharedPreferences.getInstance();
    }
  }

  reload() async {
    await _sharedPrefs?.reload();
  }

  bool? get cached => _sharedPrefs!.containsKey(Strings.is_auth_cached)
      ? _sharedPrefs!.getBool(Strings.is_auth_cached)
      : false;
  bool? get isNotificationEnabled =>
      _sharedPrefs!.containsKey(Strings.is_notification_enabled_cache)
          ? _sharedPrefs!.getBool(Strings.is_notification_enabled_cache)
          : false;
  bool? get isInspectMode =>
      _sharedPrefs!.containsKey(Strings.is_inspect_mode_cache)
          ? _sharedPrefs!.getBool(Strings.is_inspect_mode_cache)
          : false;
  bool? get isAppActive =>
      _sharedPrefs!.containsKey(Strings.is_app_active_cache)
          ? _sharedPrefs!.getBool(Strings.is_app_active_cache)
          : false;
  String? get username => _sharedPrefs!.containsKey(Strings.username_cache)
      ? _sharedPrefs!.getString(Strings.username_cache)
      : null;
  String? get appToken => _sharedPrefs!.containsKey(Strings.app_token_cache)
      ? _sharedPrefs!.getString(Strings.app_token_cache)
      : null;
  String? get notificationToken =>
      _sharedPrefs!.containsKey(Strings.notification_token_cache)
          ? _sharedPrefs!.getString(Strings.notification_token_cache)
          : null;
  String? get passHash => _sharedPrefs!.containsKey(Strings.pass_hash_cache)
      ? _sharedPrefs!.getString(Strings.pass_hash_cache)
      : null;
  int? get armDelay => _sharedPrefs!.containsKey(Strings.arm_delay_cache)
      ? _sharedPrefs!.getInt(Strings.arm_delay_cache)
      : 0;
  int? get alarmDelay => _sharedPrefs!.containsKey(Strings.alarm_delay_cache)
      ? _sharedPrefs!.getInt(Strings.alarm_delay_cache)
      : 0;
  int? get currentNotificationID =>
      _sharedPrefs!.containsKey(Strings.current_notification_id)
          ? _sharedPrefs!.getInt(Strings.current_notification_id)
          : 0;
  double? get temperature =>
      _sharedPrefs!.containsKey(Strings.system_temperature_cache)
          ? _sharedPrefs!.getDouble(Strings.system_temperature_cache)
          : 0;
  double? get humidity =>
      _sharedPrefs!.containsKey(Strings.system_humidity_cache)
          ? _sharedPrefs!.getDouble(Strings.system_humidity_cache)
          : 0;
  bool? get isCooling =>
      _sharedPrefs!.containsKey(Strings.system_is_cooling_cache)
          ? _sharedPrefs!.getBool(Strings.system_is_cooling_cache)
          : false;
  // Todo: For next update, notifications page...
  /*List<Map> get notifications => _storage.getItem(Strings.notifications_cache);*/
  set cached(bool? value) =>
      _sharedPrefs!.setBool(Strings.is_auth_cached, value!);
  set isNotificationEnabled(bool? value) =>
      _sharedPrefs!.setBool(Strings.is_notification_enabled_cache, value!);
  set username(String? value) =>
      _sharedPrefs!.setString(Strings.username_cache, value!);
  set isInspectMode(bool? value) =>
      _sharedPrefs!.setBool(Strings.is_inspect_mode_cache, value!);
  set passHash(String? value) =>
      _sharedPrefs!.setString(Strings.pass_hash_cache, value!);
  set appToken(String? value) =>
      _sharedPrefs!.setString(Strings.app_token_cache, value!);
  set notificationToken(String? value) =>
      _sharedPrefs!.setString(Strings.notification_token_cache, value!);
  set isAppActive(bool? value) =>
      _sharedPrefs!.setBool(Strings.is_app_active_cache, value!);
  set armDelay(int? value) =>
      _sharedPrefs!.setInt(Strings.arm_delay_cache, value!);
  set currentNotificationID(int? value) =>
      _sharedPrefs!.setInt(Strings.current_notification_id, value!);
  set alarmDelay(int? value) =>
      _sharedPrefs!.setInt(Strings.alarm_delay_cache, value!);
  set temperature(double? value) =>
      _sharedPrefs!.setDouble(Strings.system_temperature_cache, value!);
  set humidity(double? value) =>
      _sharedPrefs!.setDouble(Strings.system_humidity_cache, value!);
  set isCooling(bool? value) =>
      _sharedPrefs!.setBool(Strings.system_is_cooling_cache, value!);
}

final sharedPrefs = SharedPrefs();
