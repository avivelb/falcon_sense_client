import 'package:falcon_sense/classes/Sensor.dart';
import 'package:falcon_sense/classes/StringReference.dart';
import 'package:flutter/material.dart';

class GroupEditor extends StatefulWidget {
  final List<Sensor> allSensors;
  final List<Sensor> selectedSensors;
  final Function onSaveFunction;
  const GroupEditor(
      {Key? key,
      required this.allSensors,
      required this.selectedSensors,
      required this.onSaveFunction})
      : super(key: key);
  @override
  _GroupEditorState createState() => new _GroupEditorState(
      allSensors: this.allSensors,
      selectedSensors: this.selectedSensors,
      onSaveFunction: this.onSaveFunction);
}

class _GroupEditorState extends State<GroupEditor> {
  _GroupEditorState(
      {required this.allSensors,
      required this.selectedSensors,
      required this.onSaveFunction});
  final List<Sensor> allSensors;
  final List<Sensor> selectedSensors;
  final Function onSaveFunction;
  Map<Sensor, bool> _isSelected = new Map();
  void _initIsSelected() {
    allSensors.forEach((sensor) {
      _isSelected[sensor] = false;
    });
    selectedSensors.forEach((sensor) {
      _isSelected[sensor] = true;
    });
  }

  void initState() {
    super.initState();
    _initIsSelected();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
        title: Center(
          child: const Text("Group Editor"),
        ),
      ),
      Expanded(
          child: SizedBox(
              height: 200.0,
              child: ListView.separated(
                separatorBuilder: (context, index) {
                  return Divider();
                },
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      decoration: new BoxDecoration(
                        color: (_isSelected[allSensors.elementAt(index)]!)
                            ? Colors.grey[300]
                            : Colors.white,
                      ),
                      child: ListTile(
                        selected: _isSelected[allSensors.elementAt(index)]!,
                        leading: Image.asset("assets/sensor.png"),
                        title: Text(allSensors.elementAt(index).name),
                        subtitle: (allSensors.elementAt(index).type) ==
                                "Physical"
                            ? Text("GPIO ${allSensors.elementAt(index).gpIO}")
                            : Text(
                                "Length ${allSensors.elementAt(index).length}"),
                        onLongPress: () =>
                            _toggleSelection(allSensors.elementAt(index)),
                      ));
                },
                itemCount: allSensors.length,
              ))),
      FlatButton(
        onPressed: () => this.onSaveFunction(),
        child: const Text("Save"),
        color: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: Colors.black)),
      )
    ]);
  }

  void _toggleSelection(Sensor sensor) {
    setState(() {
      if (_isSelected[sensor]!) {
        _isSelected[sensor] = false;
        selectedSensors.remove(sensor);
      } else {
        _isSelected[sensor] = true;
        selectedSensors.add(sensor);
      }
    });
  }
}
