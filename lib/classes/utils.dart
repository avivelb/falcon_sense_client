import 'dart:convert';
import 'package:audioplayers/audioplayers.dart';
import 'package:crypto/crypto.dart';
import 'package:falcon_sense/classes/Group.dart';
import 'package:falcon_sense/classes/Point.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:synchronized/synchronized.dart';

typedef void GroupCallBack(Group group);

class Utils {
  static var lock = new Lock();
  static Future<void> playLocalAsset(String file) async {
    AudioPlayer audioPlayer = new AudioPlayer(playerId: "Falcon Sense Player");
    AudioCache cache = new AudioCache(fixedPlayer: audioPlayer);
    await lock.synchronized(() async {
      await cache.play(file);
      await Future.delayed(Duration(seconds: 2));
    });
  }

  static String cryptoSha512(String text) {
    return sha512.convert(utf8.encode(text)).toString();
  }

  static void toastShow(String msg) {
    Fluttertoast.showToast(msg: msg);
  }

  static Point onTap(TapDownDetails details) {
    return Point(details.globalPosition.dx, details.globalPosition.dy);
  }

  static Future<void> askDialog(BuildContext context, String title,
      String subtitle, GroupCallBack callBack, Group callbackParameter) async {
    // set up the buttons
    BuildContext? _customContext;
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        if (_customContext != null) {
          Navigator.of(_customContext!).pop();
        }
        callBack(callbackParameter);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(subtitle),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        _customContext = context;
        return alert;
      },
    );
  }

  static double celsiusToFahrenheit(double c) {
    return (9.0 / 5 * c) + 32;
  }

  static void showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static void viewGroupDialog(Group group, BuildContext context) async {
    showGeneralDialog(
      barrierLabel: "View Group",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 450,
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Center(
                      child: Text(group.name!),
                    ),
                  ),
                  Expanded(
                      child: SizedBox(
                          height: 200.0,
                          child: ListView.separated(
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title:
                                    Text(group.sensors!.elementAt(index).name),
                                onTap: () => Utils.toastShow(
                                    group.sensors!.elementAt(index).toString()),
                              );
                            },
                            itemCount: group.sensors!.length,
                          ))),
                ],
              ),
              margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position:
              Tween(begin: Offset(0.2, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }
}
